<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Inscription</title>
  <link rel="stylesheet" href="assets/css/Singin.css">
</head>
<body>
		<div class="formContainer">
		
			<img id="logo" alt="LibDiscount" src="assets/image/logoLib.png">
			<form  class="formInscription" action="CreerUtilisateur" method="post">
				<label class="labFormInscription">Nom</label>
				<input class="champFormIncription"  name="nom" type="text" placeholder="UTHMAN" required="required">
				<br>
				
				<label class="labFormInscription">Pr�nom</label>
				<input class="champFormIncription" name="prenom" type="text" placeholder="Ahmad" required="required">
				<br>
				
				<label class="labFormInscription" >T�l</label>
				<input class="champFormIncription" name="num_tel" type="tel" placeholder="0352140925" required="required">
				<br>
				
				<label class="labFormInscription" >E-mail</label>
				<input class="champFormIncription" name="email" type="email" placeholder="exemple@exemple.fr" required="required">
				<br>
				
				<hr>
				
				<label class="labFormInscription" >Nom librairie</label>
				<input class="champFormIncription" name="nom_lib" type="text" placeholder="Ronchin Librairie" required="required">
				<br>
				
				<label class="labFormInscription" >Mot de passe</label>
				<input class="champFormIncription" name="password" type="password" placeholder="********" required="required">
				<br>
				
				<label class="labFormInscription" >Confirmation</label>
				<input class="champFormIncription" name="confirmation" type="password" placeholder="********" required="required">
				<br>
				
				<label class="labFormInscription" >Numero</label>
				<input class="champFormIncription" name="numero" type="text" required="required">
				<br>
				
				<label class="labFormInscription" >Libelle</label>
				<input class="champFormIncription" name="libelle" type="text" required="required">
				<br>
				
				<label class="labFormInscription">Code postale</label>
				<input  class="champFormIncription" name="cp" type="text" required="required">
				<br>
				
				<label class="labFormInscription" >Ville</label>
				<input class="champFormIncription" name="ville" type="text" required="required">
				<br>
				
				<hr>
				
				<input type="submit" id="btnsubmit" value="S'inscrire"> 
				<a href="Login" id="singIn"> D�j� un compte ?</a>
			</form>
		</div>
</body>
</html>