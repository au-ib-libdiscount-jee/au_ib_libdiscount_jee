<%@page import="fr.afpa.beans.Utilisateur"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>LibDiscount | Mes Infos</title>
  <link rel="stylesheet" href="assets/css/Account.css">
</head>
<body>
	
	
	 <!-- Navigation -->
    <header>
      <ul>
      <li><a href="Login"><img id="logo" src="assets/image/logoLibBlanc.png"  width="220" height="70"alt=""></a></li>
      <li><a href="Deconnexion">Deconnexion</a></li>
      <li><a href="NewAnnonce">Deposer annonce</a></li>
      <li><a href="MyAnnonce">Mes Annonces</a></li>
      <li><a href="Home">Home</a></li>

      </ul>

    </header>
    
	<div class="formContainer">
	<h1>Mes information : </h1>
	<form  class="formInscription" action="UpdateUtilisateur"  method="post">
				<label class="labFormInscription">Nom</label>
				<input class="champFormIncription"  name="nom" type="text" value="<c:out value="${user.nom }"/>" required="required">
				<br>
				
				<label class="labFormInscription">Pr�nom</label>
				<input class="champFormIncription" name="prenom" type="text" value="<c:out value="${user.prenom }"/>" required="required">
				<br>
				
				<label class="labFormInscription" >T�l</label>
				<input class="champFormIncription" name="num_tel" type="tel" value="<c:out value="${user.num_tel}"/>" required="required">
				<br>
				
				<label class="labFormInscription" >E-mail</label>
				<input class="champFormIncription" name="email" type="email" value="<c:out value="${user.email}"/>" required="required">
				<br>
				
				<hr>
				
				<label class="labFormInscription" >Nom librairie</label>
				<input class="champFormIncription" name="nom_lib" type="text" value="<c:out value="${user.nom_librairie}"/>" required="required">
				<br>
				
				<label class="labFormInscription" >Numero</label>
				
				<input class="champFormIncription" name="numero" value="<c:out value="${user.getAdresse().getNumero() }"/>" type="text" required="required">
				<br>
				
				<label class="labFormInscription" >Libelle</label>
				<input class="champFormIncription" name="libelle"  value="<c:out value="${user.getAdresse().getLibelle() }"/>" type="text" required="required">
				<br>
				
				<label class="labFormInscription">Code postale</label>
				<input  class="champFormIncription" name="cp" value="<c:out value="${user.getAdresse().getCp() }"/>" required="required">
				<br>
				
				<label class="labFormInscription" >Ville</label>
				<input class="champFormIncription" name="ville" value="<c:out value="${user.getAdresse().getVille() }"/>" required="required">
				<br>
				
				<hr>
				<input type="submit" class="btnsubmit" value="Modifer"> 
				<a href="Home"><input type="button" class="btnsubmit" id="annuler" value="Annuler" ></a>
			</form>
		</div>

	
</body>
</html>