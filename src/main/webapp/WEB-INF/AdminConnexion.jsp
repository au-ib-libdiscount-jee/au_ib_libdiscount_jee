<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
 <title>LibDiscount | Login</title>
        <link rel="stylesheet" href="assets/css/LoginStyle.css">
</head>
<body>
	<table class="table" border="solid 1px black"
		style="border-spacing: 0;">
		<tr class="tr">
			
      
    	<h1>Login Admin</h1>

	
		
		
		<div class="formContainer split" >
		
			<img id="logo" alt="LibDiscount" src="assets/image/logoLib.png">
			<form action="ConnexionAdmin" method="post" class="formInscription">
				<label for="login" class="labFormInscription" >Votre Identifiant</label> 
					<input type="text" placeholder="Entrez votre login" name="login" required="required" class="champFormIncription">
				<label for="pwd" class="labFormInscription" >Votre mot de passe</label> 
					<input type="motdepasse" placeholder="Entrez votre mot de passe" name="motdepasse" required="required" class="champFormIncription">
				<button  type="submit" id="btnsubmit" >Se Connecter</button>
				
			</form>
				
		</div>
		<div class="imgLeft split" >
			<p class="citation">Portail Admin</p>
		</div>
</body>
</html>