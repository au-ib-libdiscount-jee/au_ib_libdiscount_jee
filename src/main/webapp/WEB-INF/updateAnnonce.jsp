<%@page import="java.io.File"%>
<%@page import="fr.afpa.beans.Image"%>
<%@page import="fr.afpa.beans.Annonce"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>LibDiscount | Update Annonce</title>
<link rel="stylesheet" href="assets/css/updateAnnonce.css">
</head>
<body>

	<header>
		<ul>
			<li><a href="Login"> <img id="logo" src="assets/image/logoLibBlanc.png" width="220" height="70" alt=""></a></li>
			<li><a href="Deconnexion">Deconnexion</a></li>
			<li><a href="NewAnnonce">Deposer annonce</a></li>
			<li><a href="UpdateUtilisateur">Mes infos</a></li>
			<li><a href="MyAnnonce">Mes Annonces</a></li>
			<li><a href="Home">Home</a></li>

		</ul>

	</header>
	<div class="Container">
		<form class="formAnnonce" action="UpdateAnnonce" method="post" enctype="multipart/form-data">
			<label class="labForm">Titre</label> 
			<input class="champForm" type="text" name="titre" value="<c:out value="${annonce.getTitre()}"/>">
			<br>

			<label class="labForm">ISBN</label> 
			<input class="champForm" type="text" name="isbn" value="<c:out value="${ annonce.getIsbn()}"/>"> <br>

			<label class="labForm">Maison d'�dition</label> 
			<input class="champForm" type="text" name="medition" value="<c:out value="${annonce.getMaisonEdition()}"/>">
				<br> 
				<label class="labForm">Date d'�dition</label>
				<input class="champForm"type="date" name="dedition" value="<c:out value="${annonce.getDateEdition()}"/>">
				<br>

			<label class="labForm">Niveau Scolaire</label> <select
				class="champForm" name="niveau_scolaire">
				<option value="">--Choisissez une option--</option>
				<c:choose>
					<c:when test="${annonce.niveau_scolaire == 'Maternalle'}">
						<option selected="selected"  value="Maternalle">Maternalle</option>
					</c:when>
					<c:otherwise><option value="Maternalle">Maternalle</option></c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${annonce.niveau_scolaire == '�l�mentaire'}">
						<option selected="selected"  value="�l�mentaire">�l�mentaire</option>
					</c:when>
					<c:otherwise><option value="�l�mentaire">�l�mentaire</option></c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${annonce.niveau_scolaire == 'Coll�ge'}">
						<option selected="selected"  value="Coll�ge">Coll�ge</option>
					</c:when>
					<c:otherwise><option value="Coll�ge">Coll�ge</option></c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${annonce.niveau_scolaire == 'Lyc�e'}">
						<option selected="selected"  value="Lyc�e">Lyc�e</option>
					</c:when>
					<c:otherwise><option value="Lyc�e">Lyc�e</option></c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${annonce.niveau_scolaire == 'Universit�'}">
						<option selected="selected"  value="Universit�">Universit�</option>
					</c:when>
					<c:otherwise><option value="Universit�">Universit�</option></c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${annonce.niveau_scolaire == 'Tous niveaux'}">
						<option selected="selected"  value="Tous niveaux">Tous niveaux</option>
					</c:when>
					<c:otherwise><option value="Tous niveaux">Tous niveaux</option></c:otherwise>
				</c:choose>

				</select>
				<br> 
				<label class="labForm">Prix Unitaire</label> 
				<input class="champForm" type="text" name="prix_u" value="<c:out value="${annonce.getPrixUnitaire()}"/>">
				<br> <label class="labForm">Quantit�</label> 
				<input class="champForm" type="text" name="qte" value="<c:out value="${annonce.getQuantite()}" />"> <br>

			<label class="labForm">Remise</label> <input class="champForm"
				type="text" name="remise" value=" <c:out value="${annonce.getRemise()}"/>">
			<br> <label class="labForm">Photos</label>

			<c:set var="count" value="${0}"/>
			<c:set var="dosPath" value="${request.contextPath}"/>

			<c:forEach items="${annonce.imagesList}" var="image" >
			<img alt="<c:out value="${image.getNom()}"/>" src="${dosPath}/img_annonces/${image.getNom()}"> 
			<input type="file" name="<c:out value="${image.getNom() }"/>" accept="image/png, image/jpeg">
			<br>
			
			<c:set var="count" value="${count +1 }"/>
			</c:forEach>
		
			<c:forEach var="i" begin="${count}" end="2" step="1">
			<input type="file" name="vide" accept="image/png, image/jpeg">
			</c:forEach>		
	

			<input name="id" type="hidden" value="<c:out value="${annonce.getIdAnnonce()}"/>">
			<br> <input id="btnsubmit" type="submit" value="Modifier">
		</form>
	</div>
</body>
</html>