<%@page contentType="text/html" pageEncoding="UTF-8"%>
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

    <html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>LibDiscount | Login</title>
        <link rel="stylesheet" href="assets/css/LoginStyle.css">
    </head>

    <body>

   		<div class="formContainer split" >
		
			<img id="logo" alt="LibDiscount" src="assets/image/logoLib.png">
			<form action="Login" method="post" class="formInscription">
				<label for="login" class="labFormInscription" >Votre Identifiant</label> 
					<input type="text" placeholder="Entrez votre login" name="login" required="required" class="champFormIncription">
				<label for="pwd" class="labFormInscription" >Votre mot de passe</label> 
					<input type="password" placeholder="Entrez votre mot de passe" name="pwd" required="required" class="champFormIncription">
				<button id="btnsubmit" type="submit" >Se Connecter</button>
				<a href="CreerUtilisateur" id="singIn"> Pas encore inscrit ?</a>
			</form>
				
		</div>
		<div class="imgLeft split" >
			<p class="citation">Vos livres aux meilleurs prix</p>
		</div>
 
 
    </body>
    </html>