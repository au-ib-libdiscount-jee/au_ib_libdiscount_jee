<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
     <link rel="stylesheet" href="assets/css/FormAnnonce.css">
	 <title>LibDiscount | New Annonce</title>
</head>
<body>

       <header>
      <ul>
      <li><a href="Login"><img id="logo" src="assets/image/logoLibBlanc.png"  width="220" height="70"alt=""></a></li>
      <li><a href="Deconnexion">Deconnexion</a></li>
      <li><a href="UpdateUtilisateur">Mes Infos</a></li>
      <li><a href="MyAnnonce">Mes Annonces</a></li>
      <li><a href="Home">Home</a></li>

      </ul>

    </header>
  

	<div class="formContainer">
		<h1>Ajouter Annonce</h1>
			<form action="NewAnnonce" class="formAnnonce" method="post" enctype="multipart/form-data">
			<label class="labForm">Titre</label>
			<input class="champForm" type="text" name="titre"  required="required">
			<br> 
			
			<label class="labForm">ISBN</label>
			<input  class="champForm" type="text" name="isbn" required="required">
			<br>
			
			 <label class="labForm" >Maison d'�dition</label>
			 <input class="champForm" type="text" name="medition"  required="required">
			 <br>
			 
			 <label class="labForm" >Date d'�dition</label>
			 <input class="champForm" type="date" name="dedition"  required="required">
			 <br>
			 
			 <label class="labForm" >Niveau Scolaire</label>
			 	<select  class="champForm" name="niveau_scolaire"  required="required">
				    <option value="">--Choisissez une option--</option>
				    <option value="Maternalle">Maternalle</option>
				    <option value="�l�mentaire">�l�mentaire</option>
				    <option value="Coll�ge">Coll�ge</option>
				    <option value="Lyc�e">Lyc�e</option>
				    <option value="Universit�">Universit�</option>
				    <option value="Tous niveaux">Tous niveaux</option>
				</select>
			<br> 
				
			<label class="labForm">Prix Unitaire</label>
			<input class="champForm" type="text" name="prix_u"  required="required">
			<br>
			
			<label class="labForm">Quantit�</label>
			<input  class="champForm" type="text" name="qte"  required="required">
			<br>
			
			<label class="labForm">Remise</label>
			<input class="champForm" type="text" name="remise"  required="required">
			<br>
			
			<label class="labForm">Photos</label>
			<input type="file" name="img1" accept="image/png, image/jpeg">
			<input type="file" name="img2" accept="image/png, image/jpeg">
			<input type="file" name="img3" accept="image/png, image/jpeg">
			<br> 
			
			<input id="btnsubmit" type="submit" value="Publier">
			
		</form>
	</div>
</body>
</html>