<%@page import="fr.afpa.beans.Annonce"%>
<%@page import="java.util.List"%>
<%@page import="fr.afpa.beans.Utilisateur"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="assets/css/MyAnnonce.css">
<title>LibDiscount | Mes Annonces</title>
</head>
<body>

	
	<header>
	    <ul>
		     <li><a href="Login"><img id="logo" src="assets/image/logoLibBlanc.png"  width="220" height="70"alt=""></a></li>
		     <li><a class="a" href="Deconnexion">Deconnexion</a></li>
		     <li><a class="a" href="NewAnnonce">Deposer annonce</a></li>
		     <li><a class="a" href="UpdateUtilisateur">Mes infos</a></li>
		     <li><a class="a" href="MyAnnonce">Mes Annonces</a></li>
		     <li><a class="a" href="Home">Home</a></li>
	    </ul>
	</header>
	
	
	<c:set var="path" value="${request.contextPath}/img_annonces/"/>
	<c:set var="params" value="MyAnnonce"/>
	<c:if test='${filter != null }'>
		<c:set var="params" value="SearchServlet"/>
	</c:if>	
	
	
	<div class="main-container">
		<div class="annonces-container">
			
			
			<c:if test="${annonces.isEmpty()}">
				<h1>Il n'y a pas de resultats</h1>
			</c:if>
			<c:forEach items="${annonces}" var="a">
			<div class="annonce">
				<div class="div-img">
					<c:choose>
						<c:when test="${a.imagesList.isEmpty()}">
							<img src="assets/image/vide.svg" alt="vide">
						</c:when>
						<c:otherwise>
							<c:url value="${path}${a.imagesList.get(0).nom}" var="img"/>
						<img src="${img}" alt="imageAnnonce">
						</c:otherwise>
					</c:choose>
				</div>
				<div class="div-right">
					<div class="div-title">
							<p class="title"><c:out value="${a.titre}"/></p>
					</div>
					<div class="div-info">
						<div class="info-annonce">
							<p class="info"><b>Date d'édition : </b><c:out value="${a.dateEdition}"/></p>
							<p class="info"><b>Maison d'édition : </b><c:out value="${a.maisonEdition}"/></p>
							<p class="info"><b>ISBN : </b><c:out value="${a.isbn}"/></p>
							<p class="info"><b>Niveau Scolaire : </b><c:out value="${a.niveau_scolaire}"/></p>
							<p class="info"><b>Quantité : </b><c:out value="${a.quantite}"/>pcs</p>
							<p class="info"><b>Prix unitaire : </b><c:out value="${a.prixUnitaire}"/>€</p>
							<p class="info"><b>Remise : </b><c:out value="${a.remise}"/>%</p>		
						</div>
						<div class="div-service">
							<c:url value="UpdateAnnonce" var="modifier">
								<c:param name="id" value="${a.idAnnonce}"></c:param>
							</c:url>
							<a href="${modifier}"><input class="btn" type="button" value="Modifier"></a><br>
							<c:url value="SupprimerAnnonce" var="supprimer">
								<c:param name="id" value="${a.idAnnonce}"></c:param>
							</c:url>
							<a href="${supprimer}"><input class="btn" type="button" value="Supprimer"></a>
						</div>
					</div>
					<div class="div-sud" >
						<p class="dateA">piblié le <c:out value="${a.dateAnnonce}"/></p>
						<hr class="line">
						<p class="prix">Prix TOTAL : <c:out value="${a.prixUnitaire*a.quantite*(1-a.remise/100) }"/>€</p>
					</div>
				</div>
	  		</div>
			</c:forEach>	
			<div class="div-pagination">
				<c:if test="${cPage > 1 }">
					<c:url value="${params}" var="prev">
						<c:if test='${filter != null }'>
							<c:param name="filter" value="${filter}"/>
							<c:param name="value" value="${value}"/>
						</c:if>
						<c:param name="page" value="${cPage -1 }"></c:param>
					</c:url>
					<a class="next" href= "${prev}"><button class="btn">Previous</button></a>
				</c:if>
				<c:if test="${cPage < pageMax }">
					<c:url value="${params}" var="next">
						<c:if test='${filter != null }'>
							<c:param name="filter" value="${filter}"/>
							<c:param name="value" value="${value}"/>
						</c:if>
						<c:param name="page" value="${cPage +1 }"></c:param>
					</c:url>
					<a class="past" href= "${next}"><button class="btn">NEXT</button></a>
				</c:if>
			</div>	
		</div>
	</div>
</body>
</html>