package fr.afpa.metiers;

import fr.afpa.beans.Utilisateur;
import fr.afpa.dao.UtilisateurDAO;

public class UtilisateurMetier {
	private UtilisateurDAO dao;

	public UtilisateurMetier() {
		dao = new UtilisateurDAO();
	}

	public void addUtilisateur(Utilisateur utilisateur) {
		dao.addUtilisateur(utilisateur);
	}

	public void updateUtilisateur(Utilisateur user) {
		dao.updateUtilisateur(user);
	}
	
	public Utilisateur getUtilisateurPyId(int id) {
		return dao.getUtilisateurPyID(id);
	}
}
