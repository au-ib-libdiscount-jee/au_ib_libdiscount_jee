package fr.afpa.metiers;

import fr.afpa.beans.Admin;
import fr.afpa.beans.Auth;
import fr.afpa.beans.Auth_admin;
import fr.afpa.beans.Utilisateur;
import fr.afpa.dao.AuthAdminDAO;


public class AuthAdminMetiers {

	public Auth_admin getAuth(Admin admin) {
		return new AuthAdminDAO().getAuth(admin);
	}

	public Auth_admin getAuth(String email, String password) {
		return new AuthAdminDAO().getAuth(email, password);
	}

	public void addAuth(Auth_admin auth) {
		new AuthAdminDAO().createAuth(auth);

	}

	public void updateAuth(Auth_admin auth) {
		new AuthAdminDAO().updateAuth(auth);
	}
}
