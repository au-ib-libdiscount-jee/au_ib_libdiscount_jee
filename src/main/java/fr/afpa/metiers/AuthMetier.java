package fr.afpa.metiers;

import fr.afpa.beans.Auth;
import fr.afpa.beans.Utilisateur;
import fr.afpa.dao.AuthDAO;

public class AuthMetier {

	public Auth getAuth(Utilisateur user) {
		return new AuthDAO().getAuth(user);
	}

	public Auth getAuth(String email, String password) {
		return new AuthDAO().getAuth(email, password);
	}

	public void addAuth(Auth auth) {
		new AuthDAO().createAuth(auth);

	}

	public void updateAuth(Auth auth) {
		new AuthDAO().updateAuth(auth);
	}

	public Auth getAuthById(int id) {
		return new AuthDAO().getAuthByIdUser(id);
	}
}
