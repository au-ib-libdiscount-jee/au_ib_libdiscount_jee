package fr.afpa.metiers;

import java.util.List;

import fr.afpa.beans.Annonce;
import fr.afpa.dao.AnnonceDAO;

/**
 * 
 * @author AU_LIBDISCOUNT
 *
 */
public class AnnonceMetier {

	private AnnonceDAO dao;

	public AnnonceMetier() {
		dao = new AnnonceDAO();
	}

	public void addAnnonce(Annonce annonce) {
		dao.addAnnonce(annonce);
	}

	public List<Annonce> listerAnnonce() {
		return dao.listerAnnonce();
	}

	public void updateAnnonce(Annonce annonce) {
		dao.updateAnnonce(annonce);
	}

	public void supprimerAnnonce(Annonce annonce) {
		dao.supprimerAnnonce(annonce);
	}

	public List<Annonce> searchAnnonceParISBN(String isbn) {
		return dao.searchAnnonceParISBN(isbn);
	}
/********************* */
	public Annonce searchAnnonceParId(int id) {
		return dao.searchAnnonceParId(id);
	}
/********************* */
	public List<Annonce> searchAnnonceParVille(String ville) {
		return dao.searchAnnonceParVille(ville);
	}

	public List<Annonce> searchAnnonceParTitre(String motCle) {
		return dao.searchAnnonceParTitre(motCle);
	}
	
	public List<Annonce> searchAnnonceByIdUser(int id){
		return dao.searchByIdUser(id);
	}

	public Annonce getAnnonceByID(int id) {
		return dao.getAnnonceByID(id);
	}

	public List<Annonce> paginationAnnonce(int start){
		return dao.paginationAnnonce(start);
	}
	
	public long countAnnonce() {
		return dao.countAnnonce();
	}
	
	public List<Annonce> paginationAnnonce(int start,String filter,String value){
		return dao.paginationAnnonce(start,filter,value);
	}
	
	public List<Annonce> paginationAnnonceByUser(int start, int idUser) {
		return dao.paginationAnnonceByUser(start, idUser);
	}
	
	public List<Annonce> listerAnnonceByUser(int id) {
		return dao.listerAnnonceByUser(id);
	}
	
	public long countAnnonce(String filter,String value) {
		return dao.countAnnonce(filter,value);
	}

	
}
