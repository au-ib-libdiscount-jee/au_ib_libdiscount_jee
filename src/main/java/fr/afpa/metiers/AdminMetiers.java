package fr.afpa.metiers;

import java.util.List;

import fr.afpa.beans.Admin;
import fr.afpa.beans.Utilisateur;
import fr.afpa.dao.AdminDAO;

public class AdminMetiers {

	private AdminDAO dao  = new AdminDAO();
	
	
	public void addAdmin(Admin admin) {
		
		 dao.addAddmin(admin);
	}
	

	public void updateAdmin(Admin admin) {
		dao.updateAdmin(admin);
	}
	public void deleteById(int id) {
		dao.DeletById(id);
	}
	public void desactiver(int id) {
		dao.desactiver(id);
	}
	public Utilisateur searchUserById(int id) {
		return dao.getUtilisateurPyID(id);
	}
	public void updateAdlin(Admin admin) {
		dao.updateAdmin(admin);
	}
	public List<Utilisateur> listerUtilisateur() {
		return dao.listeUtilisateur();
	}
	
}
