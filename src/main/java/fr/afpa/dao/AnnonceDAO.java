package fr.afpa.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.beans.Annonce;
import fr.afpa.utils.HibernateUtils;

/**
 * 
 * @author AU_LIBDISCOUNT
 *
 */
public class AnnonceDAO {

	/**
	 * Pour enregistrer une annonce dans la base de donn�es
	 * 
	 * @param annonce � enregistrer
	 */
	public void addAnnonce(Annonce annonce) {

		Session session = HibernateUtils.getSession();
		Transaction tx = session.beginTransaction();
		session.update(annonce.getUtilisateur());
		session.persist(annonce);
		tx.commit();
		session.close();
	}

	/**
	 * Pour mettre � jour une annonce dans la base de donn�es
	 * 
	 * @param annonce � mettre � jour
	 */
	public void updateAnnonce(Annonce annonce) {

		Session session = HibernateUtils.getSession();
		Transaction tx = session.beginTransaction();
		session.update(annonce);
		tx.commit();
		session.close();
	}

	/**
	 * Pour supprimer une annonce de la base de donn�es
	 * 
	 * @param annonce � supprimer
	 */
	public void supprimerAnnonce(Annonce annonce) {

		Session session = HibernateUtils.getSession();
		Transaction tx = session.beginTransaction();
		session.delete(annonce);
		tx.commit();
		session.close();
	}

	/**
	 * Pour lister toutes les annonces enregistr�es dans la base de donn�es
	 * 
	 * @return liste de toutes les annonces dans la base de donn�es
	 */
	public List<Annonce> listerAnnonce() {

		Session session = HibernateUtils.getSession();
		Query req = session.createQuery("from Annonce");
		@SuppressWarnings("unchecked")
		List<Annonce> listeAnnonce = (List<Annonce>) req.getResultList();
		session.close();
		return listeAnnonce;

	}
	
	/**
	 * Pour lister toutes les annonces enregistrees dans la base de donn�es pour un utilisateur
	 * 
	 * @return liste de toutes les annonces dans la base de donn�es
	 */
	public List<Annonce> listerAnnonceByUser(int id) {
		
		Session session = HibernateUtils.getSession();
		Query req = session.getNamedQuery("findAllById");
		req.setParameter("id", id);
		@SuppressWarnings("unchecked")
		List<Annonce> listeAnnonce = (List<Annonce>) req.getResultList();
		session.close();
		return listeAnnonce;
		
	}

	/**
	 * Pour rechercher des annonce en fonction de mots cl� titre
	 * 
	 * @param titre le mot cl� a rechercher
	 * @return liste d'annonces contient de les mots cl� dans le titre
	 */
	public List<Annonce> searchAnnonceParTitre(String titre) {

		Session session = HibernateUtils.getSession();
		Query req = session.getNamedQuery("findByTitre");
		req.setParameter("motCle", "%" + titre + "%");
		@SuppressWarnings("unchecked")
		List<Annonce> listeAnnonce = (List<Annonce>) req.getResultList();
		session.close();
		return listeAnnonce;
	}

	/**
	 * Pour Rechercher les annonces en fonction de ISBN
	 * 
	 * @param isbn � rechercher
	 * @return liste des annonces portent le ISBN recherch�
	 */
	public List<Annonce> searchAnnonceParISBN(String isbn) {

		Session session = HibernateUtils.getSession();
		Query req = session.getNamedQuery("findByISBN");
		req.setParameter("isbn", isbn);
		@SuppressWarnings("unchecked")
		List<Annonce> list = req.getResultList();
		session.close();

		return list;

	}

	/**
	 * Pour rechercher les annonces publi�es dans une ville donn�e
	 * 
	 * @param ville � rechercher
	 * @return Liste des annonces publi�es dans la ville precis�e en paramettre
	 */
	public List<Annonce> searchAnnonceParVille(String ville) {
		Session session = HibernateUtils.getSession();
		Query req = session.getNamedQuery("findByVille");
		req.setParameter("ville", ville);
		@SuppressWarnings("unchecked")
		List<Annonce> annonce = req.getResultList();
		session.close();
		return annonce;
	}

	public Annonce getAnnonceByID(int id) {
		Session session = HibernateUtils.getSession();
		Query req = session.getNamedQuery("findByIdAnnonce");
		req.setParameter("id", id);
		Annonce annonce = req.getResultList().isEmpty() ? null : (Annonce) req.getResultList().get(0);
		session.close();
		return annonce;
	}

	/**************************************** */

	public Annonce searchAnnonceParId(int id) {

		Session session = HibernateUtils.getSession();
		Query req = session.getNamedQuery("findByIdAnnonce");
		req.setParameter("id", id);
		Annonce annonce = (Annonce) req.getSingleResult();
		session.close();

		return annonce;

	}

	/******************************************* */

	public List<Annonce> searchByIdUser(int id) {

		Session session = HibernateUtils.getSession();
		Query req = session.getNamedQuery("findAllById");
		req.setParameter("id", id);
		@SuppressWarnings("unchecked")
		List<Annonce> listeAnnonce = (List<Annonce>) req.getResultList();
		session.close();
		return listeAnnonce;
	}

	/**
	 * Pour rechercher les annonces avec un d�but de rechercher param�trable
	 * 
	 * @param start : le d�but de notre r�cherche
	 * @return Liste d'annonce
	 */
	public List<Annonce> paginationAnnonce(int start) {
		Session session = HibernateUtils.getSession();
		Query req = session.createQuery("From Annonce");
		req.setFirstResult(start);
		req.setMaxResults(3);
		@SuppressWarnings("unchecked")
		List<Annonce> annonces = req.getResultList();
		session.close();
		return annonces;

	}

	@SuppressWarnings("unchecked")
	public List<Annonce> paginationAnnonce(int start, String filter, String value) {
		Session session = HibernateUtils.getSession();
		Query req = null;
		switch (filter) {
		case "keyWord":
			req = session.getNamedQuery("findByTitre");
			req.setParameter("motCle", "%" + value + "%");
			break;
		case "isbn":
			req = session.getNamedQuery("findByISBN");
			req.setParameter("isbn", value);
			break;
		case "city":
			req = session.getNamedQuery("findByVille");
			req.setParameter("ville", value);
			break;

		default:
			break;
		}
		
		List<Annonce> annonces = new ArrayList<Annonce>();
		if(req != null) {
			req.setFirstResult(start);
			req.setMaxResults(3);
			annonces = req.getResultList();
		}
		session.close();
		return annonces;

	}
	
	public List<Annonce> paginationAnnonceByUser(int start, int idUser){
		Session session = HibernateUtils.getSession();
		Query req = session.createQuery("From Annonce a  where a.utilisateur.id_user =: id");
		req.setParameter("id", idUser);
		req.setFirstResult(start);
		req.setMaxResults(5);
		@SuppressWarnings("unchecked")
		List<Annonce> annonces = req.getResultList();
		session.close();
		return annonces;
		
	}

	/**
	 * Permet de renvoyer le nombre d'annonce enregister en base de donn�es
	 * 
	 * @return nbRow le nombre d'annonce enregistrer en base
	 */
	public long countAnnonce() {

		Session session = HibernateUtils.getSession();
		Query req = session.createQuery("Select count (a.id) From Annonce a");
		long nbRow = (long) req.getSingleResult();
		session.close();
		return nbRow;

	}

	public long countAnnonce(String filter, String value) {

		Session session = HibernateUtils.getSession();
		Query req = session.createQuery("Select count (a.id) From Annonce a");

		switch (filter) {
		case "keyWord":
			req = session.createQuery("Select count (a.id) From Annonce a where lower(a.titre) like :value ");
			req.setParameter("value", "%" + value + "%");
			break;
		case "isbn":
			req = session.createQuery("Select count (a.id) From Annonce a where a.isbn = :value ");
			req.setParameter("value", value);
			break;
		case "city":
			req = session.createQuery("Select count (a.id) From Annonce a where lower(a.utilisateur.adresse.ville) = :value ");
			req.setParameter("value", value);
			break;

		default:
			break;
		}
		long nbRow = (long) ((req.getResultList().isEmpty()) ? 0L : req.getResultList().get(0));
		session.close();
		return nbRow;
	}
}
