package fr.afpa.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.beans.Utilisateur;
import fr.afpa.utils.HibernateUtils;

/**
 * 
 * @author AU_LibDiscount
 *
 */
public class UtilisateurDAO {

	/**
	 * Pour Enregistrer un utilisateur dans la base de donn�es
	 * 
	 * @param utilisateur � enregistrer dans la base de donn�es
	 */
	public void addUtilisateur(Utilisateur utilisateur) {
		Session session = HibernateUtils.getSession();
		Transaction tx = session.beginTransaction();
		session.persist(utilisateur);
		tx.commit();
		session.close();
	}

	/**
	 * Pour mettre ajour un utilisateur dans l base de donn�es
	 * 
	 * @param utilisateur � mettre � jour
	 */
	public void updateUtilisateur(Utilisateur utilisateur) {
		Session session = HibernateUtils.getSession();
		Transaction tx = session.beginTransaction();
		session.update(utilisateur);
		tx.commit();
		session.close();
	}

	/**
	 * Pour recupper tous les utilisateur enregistr�s dans la base de donn�es
	 * 
	 * @return la liste de tous les utilisateurs enregistr�s dans la base de donn�es
	 */
	public List<Utilisateur> listerUtilisateur() {
		Session session = HibernateUtils.getSession();
		Query req = session.getNamedQuery("allUser");
		@SuppressWarnings("unchecked")
		List<Utilisateur> listeUtilisateur = (List<Utilisateur>) req.getResultList();
		session.close();
		return listeUtilisateur;
	}

	/**
	 * Pour recupperer un utilisateur de la base de donn�es en fonction de son
	 * id_user
	 * 
	 * @param id de l'utilisateur � recupperer
	 * @return l'utilisateur ou null si il n'existe pas
	 */
	public Utilisateur getUtilisateurPyID(int id) {
		Session session = HibernateUtils.getSession();
		Query req = session.getNamedQuery("FindByIdUser");
		req.setParameter("id",id);
		Utilisateur utilisateur = (Utilisateur) req.getSingleResult();
		session.close();
		return utilisateur;
	}

}
