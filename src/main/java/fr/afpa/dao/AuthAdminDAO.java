package fr.afpa.dao;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.beans.Admin;
import fr.afpa.beans.Auth;
import fr.afpa.beans.Auth_admin;
import fr.afpa.beans.Utilisateur;
import fr.afpa.utils.HibernateUtils;

public class AuthAdminDAO {


	public void createAuth(Auth_admin auth) {
		Session session = HibernateUtils.getSession();
		Transaction tx = session.beginTransaction();
		session.persist(auth);
		tx.commit();
		session.close();
	}

	/**
	 * Pour verfier lautentification en fonction de l'email et le mot de passe
	 * 
	 * @param email    � rechercher et verifier
	 * @param password � rechercher et verifier
	 * @return un auth ou null si il n'existe pas
	 */
	public Auth_admin getAuth(String login, String password) {

		Session session = HibernateUtils.getSession();
		Query req = session.getNamedQuery("findByLoginAndPassword");
		req.setParameter("login", login);
		req.setParameter("password", password);
		Auth_admin auth = (Auth_admin) req.getSingleResult();
		session.close();
		return auth;
	}

	/**
	 * Pour recupper un Auth de la base de donn�es en fonction de ul'admin
	 * 
	 * @param admin � recuperer son Auth
	 * @return l'auth ou null s'il n'existe pas
	 */
	public Auth_admin getAuth(Admin admin) {

		Session session = HibernateUtils.getSession();
		Query req = session.getNamedQuery("findByUser");
		req.setParameter("admin", admin);
		Auth_admin auth = (Auth_admin) req.getSingleResult();
		session.close();
		return auth;
	}

	/**
	 * Pour mettre � jour un Auth dans la base de donn�es
	 * 
	 * @param auth � mettre � jour
	 */
	public void updateAuth(Auth_admin auth) {

		Session session = HibernateUtils.getSession();
		Transaction tx = session.beginTransaction();
		session.update(auth);
		tx.commit();
		session.close();

	}

}
