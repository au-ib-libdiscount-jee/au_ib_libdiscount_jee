package fr.afpa.dao;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.beans.Auth;
import fr.afpa.beans.Utilisateur;
import fr.afpa.utils.HibernateUtils;

/**
 * 
 * @author AU_LIBDISCOUNT
 *
 */
public class AuthDAO {

	/**
	 * Pour enregistrer un Auth dans la base de donn�es
	 * 
	 * @param auth � enregistrer dans la base de donn�es
	 */
	public void createAuth(Auth auth) {
		Session session = HibernateUtils.getSession();
		Transaction tx = session.beginTransaction();
		session.persist(auth);
		tx.commit();
		session.close();
	}

	/**
	 * Pour verfier lautentification en fonction de l'email et le mot de passe
	 * 
	 * @param email    � rechercher et verifier
	 * @param password � rechercher et verifier
	 * @return un auth ou null si il n'existe pas
	 */
	public Auth getAuth(String email, String password) {

		Session session = HibernateUtils.getSession();
		Query req = session.getNamedQuery("findByEmailAndPassword");
		req.setParameter("email", email);
		req.setParameter("password", password);
		Auth auth = req.getResultList().isEmpty() ? null : (Auth) req.getResultList().get(0);
		session.close();
		return auth;
	}

	/**
	 * Pour recupper un Auth de la base de donn�es en fonction de utilisateur
	 * 
	 * @param user � recuperer son Auth
	 * @return l'auth ou null s'il n'existe pas
	 */
	public Auth getAuth(Utilisateur user) {

		Session session = HibernateUtils.getSession();
		Query req = session.getNamedQuery("findByUser");
		req.setParameter("user", user);
		Auth auth = req.getResultList().isEmpty() ? null : (Auth) req.getResultList().get(0);
		session.close();
		return auth;
	}

	/**
	 * Pour mettre � jour un Auth dans la base de donn�es
	 * 
	 * @param auth � mettre � jour
	 */
	public void updateAuth(Auth auth) {

		Session session = HibernateUtils.getSession();
		Transaction tx = session.beginTransaction();
		session.update(auth);
		tx.commit();
		session.close();

	}

	public Auth getAuthByIdUser(int id) {
		Session session = HibernateUtils.getSession();
		Query req = session.getNamedQuery("FindAuthByIdUser");
		req.setParameter("id", id);
		Auth auth = req.getResultList().isEmpty() ? null : (Auth) req.getResultList().get(0);
		session.close();
		return auth;
	}

}
