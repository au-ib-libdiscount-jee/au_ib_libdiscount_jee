package fr.afpa.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.beans.Admin;
import fr.afpa.beans.Annonce;
import fr.afpa.beans.Auth;
import fr.afpa.beans.Utilisateur;
import fr.afpa.utils.HibernateUtils;

/**
 * 
 * @author AU_LIBDISCOUNT
 *
 */
public class AdminDAO {
	/**
	 *Ajoute un admin
	 *@param Admin a ajouter
	 */
	public void addAddmin(Admin admin) {
	
		Session session = HibernateUtils.getSession();
		Transaction tx = session.beginTransaction();
		session.persist(admin);
		tx.commit();
		session.close();
	}
	/**
	 *Update l'admin
	 *@param Admin a ajouter
	 */
	public void updateAdmin(Admin admin) {
		Session session = HibernateUtils.getSession();
		Transaction tx = session.beginTransaction();
		session.update(admin);
		tx.commit();
		session.close();
	}
	
	/**
	 *@return utilsateur grace � l'id pris en parametre
	 *@param  l'id user
	 */
	 
	public Utilisateur getUtilisateurPyID(int id) {
		Session session = HibernateUtils.getSession();
		Query req = session.getNamedQuery("FindByIdUser");
		Utilisateur utilisateur = (Utilisateur) req.getSingleResult();
		session.close();
		return utilisateur;
	}
	/**
	 *Supprimer le user
	 *@param L'id de l'user � supprimer
	 */
	public void DeletById(int id) {
	
		Session session = HibernateUtils.getSession();
		Transaction tx = session.beginTransaction();
		Query req = session.getNamedQuery("FindByIdUser");
		Utilisateur user = (Utilisateur) req.getSingleResult();
		session.delete(user);
		tx.commit();
		session.close();
	}
	/**
	 *@return listAnnonce
	 */
	public List<Annonce> listerAnnonce() {
		Session session = HibernateUtils.getSession();
		Query req = session.getNamedQuery("allAnnonce");
		@SuppressWarnings("unchecked")
		List<Annonce> listeAnnonce = (List<Annonce>) req.getResultList();
		session.close();
		return listeAnnonce;
	}
	/**
	 *@return listUser
	 */
	public List<Utilisateur> listeUtilisateur() {
		Session session = HibernateUtils.getSession();
		Query req = session.getNamedQuery("allUser");
		@SuppressWarnings("unchecked")
		List<Utilisateur> listeUtilisateur = (List<Utilisateur>) req.getResultList();
		session.close();
		return listeUtilisateur;
	}
	/**
	 *Set le isActive en false grace � l'id de l'user concern�
	 */
	public void desactiver(int id) {
		Session session = HibernateUtils.getSession();
		Transaction tx = session.beginTransaction();
		Query req = session.getNamedQuery("FindAuthByIdUser");
		req.setParameter("id", id);
		Auth auth = (Auth) req.getSingleResult();
		auth.setActive(false);
		session.update(auth);
		System.out.print("////////////////"+auth.isActive());
		session.close();
		
		
	}
}
