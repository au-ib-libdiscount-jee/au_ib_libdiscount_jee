package fr.afpa.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import fr.afpa.beans.Annonce;
import fr.afpa.beans.Utilisateur;
import fr.afpa.metiers.AnnonceMetier;

/**
 * 
 * @author AU_LIBDISCOUNT
 *
 */
/**
 * Servlet implementation class MyAnnonce
 */
public class MyAnnonce extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	public MyAnnonce() {
		super();
	}

	/**
	 * Affiche la page annonce ainsi que toute les annonce de l'user
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		int page = 1;
		int nbPages = 0;
		AnnonceMetier am = new AnnonceMetier();
		Utilisateur user = (Utilisateur) request.getSession().getAttribute("user");
		
		if (user != null) {
	
			if (request.getParameter("page") != null) {

				if(request.getParameter("page").matches("[0-9]{1,3}")) {

					page = Integer.parseInt(request.getParameter("page"));
					
					
				}

			}
			
			List<Annonce> listAnnonceUser = am.listerAnnonceByUser(user.getId_user());
			request.setAttribute("cPage", page);
			
			nbPages = ((int) (Math.ceil((double) (listAnnonceUser.size()+1)/ 5)));

			List<Annonce> listAnnonces = am.paginationAnnonceByUser((page - 1) * 5, user.getId_user());

			request.setAttribute("annonces", listAnnonces);
			request.setAttribute("pageMax", nbPages);

			getServletContext().getRequestDispatcher("/WEB-INF/MyAnnonce.jsp").forward(request, response);

		} else {
	
			getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
		}

	}

}
