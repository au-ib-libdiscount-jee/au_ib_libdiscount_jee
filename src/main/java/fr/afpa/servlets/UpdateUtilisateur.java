package fr.afpa.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.beans.Utilisateur;
import fr.afpa.metiers.UtilisateurMetier;

/**
 * 
 * @author AU_LIBDISCOUNT
 *
 */
/**
 * Servlet implementation class UpdateUtilisateur
 */
public class UpdateUtilisateur extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UpdateUtilisateur() {
		super();

	}

	/**
	 * Recupere la session user
	 *  @return la page account
	 *  @param la session user en cours
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Utilisateur user = (Utilisateur) request.getSession().getAttribute("user");
		if (user != null) {
			getServletContext().getRequestDispatcher("/WEB-INF/Account.jsp").forward(request, response);
		} else {
			response.sendRedirect("index.jsp");
		}

	}

	/**
	 * Remplace les info utilisateur par les nouveau renseigner dans les champs
	 *  @param les ifno update de l'user
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Utilisateur user = (Utilisateur) request.getSession().getAttribute("user");
		if (user != null) {
			user.setNom(request.getParameter("nom"));
			user.setPrenom(request.getParameter("prenom"));
			user.setEmail(request.getParameter("email"));
			user.setNom_librairie(request.getParameter("nom_lib"));
			user.setNum_tel(request.getParameter("num_tel"));
			user.getAdresse().setNumero(request.getParameter("numero"));
			user.getAdresse().setLibelle(request.getParameter("libelle"));
			user.getAdresse().setCp(request.getParameter("cp"));
			user.getAdresse().setVille(request.getParameter("ville"));

			new UtilisateurMetier().updateUtilisateur(user);

			response.sendRedirect("Home");
		} else {
			response.sendRedirect("index.jsp");
		}
	}

}
