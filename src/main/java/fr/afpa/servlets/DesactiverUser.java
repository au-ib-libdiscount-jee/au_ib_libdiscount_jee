package fr.afpa.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.beans.Auth;
import fr.afpa.dao.AuthDAO;
import fr.afpa.metiers.AdminMetiers;
import fr.afpa.metiers.AuthMetier;

/**
 * 
 * @author AU_LIBDISCOUNT
 *
 */
/**
 * Servlet implementation class DesactiverUser
 */
public class DesactiverUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
    public DesactiverUser() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * Appel le constructeur authMetier et desactive l'utilisateur que l'on r�cupere grace � l'id
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		AuthMetier am = new AuthMetier();
		Auth auth = am.getAuthById(id);
		if (auth.isActive() == true) {
		auth.setActive(false);
		am.updateAuth(auth);
		}
		
		
		System.out.println("///////////////// ID = "+id);
		
		
	}

	
	

}
