package fr.afpa.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.beans.Auth_admin;
import fr.afpa.beans.Utilisateur;
import fr.afpa.metiers.AdminMetiers;
import fr.afpa.metiers.AuthAdminMetiers;
import fr.afpa.beans.Admin;

/**
 * 
 * @author AU_LIBDISCOUNT
 *
 */
/**
 * Servlet implementation class ConnexionAdmin
 */
public class ConnexionAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
    
public ConnexionAdmin() {
      super();
      // TODO Auto-generated constructor stub
  }

	
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	  RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/AdminConnexion.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * recupere l'auth li� � l'admin dans la bdd 
	 *  @return liste des user
	 *  @param la session admin en cours
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	
	 Auth_admin auth = new AuthAdminMetiers().getAuth(request.getParameter("login"), request.getParameter("motdepasse"));
	 
	 if(auth != null) {
		
		 Admin admin =  auth.getAdmin();
		
		 HttpSession session = request.getSession();
		
		 session.setAttribute("admin", admin);
			ArrayList<Utilisateur> listP = new ArrayList<Utilisateur>();
			AdminMetiers adm = new AdminMetiers();
			listP = (ArrayList<Utilisateur>) adm.listerUtilisateur();
			request.setAttribute("listeUtilisateur", listP);

		 getServletContext().getRequestDispatcher("/WEB-INF/DashboardAdmin.jsp").forward(request, response);
	 }
	 
		
	}


}



