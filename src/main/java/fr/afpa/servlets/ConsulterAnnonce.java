package fr.afpa.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Auth;
import fr.afpa.beans.Utilisateur;
import fr.afpa.metiers.AnnonceMetier;
import fr.afpa.metiers.AuthMetier;
import fr.afpa.metiers.UtilisateurMetier;

/**
 * 
 * @author AU_LIBDISCOUNT
 *
 */
/**
 * Servlet implementation class ConsulterAnnonce
 */
public class ConsulterAnnonce extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
    public ConsulterAnnonce() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 *Permet de consulter les annonce d'un user auquel on aura r�cuprer l'id
	 *@return liste d'annonce
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		AnnonceMetier am = new AnnonceMetier();
		UtilisateurMetier um = new UtilisateurMetier();
		List<Annonce> listA = new ArrayList<Annonce>();
		
		listA = am.searchAnnonceByIdUser(id);
		request.setAttribute("listAnnonce", listA);
		Utilisateur user = um.getUtilisateurPyId(id);
		request.setAttribute("user", user);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/InfoUser.jsp");
		dispatcher.forward(request, response);
		
	

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
