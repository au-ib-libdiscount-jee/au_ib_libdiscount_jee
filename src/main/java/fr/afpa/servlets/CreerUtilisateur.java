package fr.afpa.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.beans.Adresse;
import fr.afpa.beans.Auth;
import fr.afpa.beans.Utilisateur;
import fr.afpa.metiers.AuthMetier;
import fr.afpa.metiers.UtilisateurMetier;

/**
 * Servlet implementation class CreerUtilisateur
 */
public class CreerUtilisateur extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CreerUtilisateur() {
		super();
		// TODO Auto-generated constructor stub
	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		getServletContext().getRequestDispatcher("/WEB-INF/Inscription.jsp").forward(request, response);
		;
	}

	/**
	 * Creer puis passe en parametre un user en lui attribuant une auth
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Adresse adresse = new Adresse();
		adresse.setNumero(request.getParameter("numero"));
		adresse.setLibelle(request.getParameter("libelle"));
		adresse.setCp(request.getParameter("cp"));
		adresse.setVille(request.getParameter("ville"));

		Utilisateur user = new Utilisateur();
		user.setNom(request.getParameter("nom"));
		user.setPrenom(request.getParameter("prenom"));
		user.setNum_tel(request.getParameter("num_tel"));
		user.setEmail(request.getParameter("email"));
		user.setNom_librairie(request.getParameter("nom_lib"));
		user.setAdresse(adresse);
		new UtilisateurMetier().addUtilisateur(user);
		new AuthMetier().addAuth(new Auth(user, user.getEmail(), request.getParameter("password"), true));
		response.sendRedirect("index.jsp");
	}

}
