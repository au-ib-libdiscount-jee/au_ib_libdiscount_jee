package fr.afpa.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.beans.Annonce;
import fr.afpa.metiers.AnnonceMetier;

/**
 * 
 * @author AU_LIBDISCOUNT
 *
 */
/**
 * Servlet implementation class Home
 */
public class Home extends HttpServlet {
	private static final long serialVersionUID = 1L;


	public Home() {
		super();

	}

	/**
	 * Lance la homepage de l'user
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (request.getSession().getAttribute("user") != null) {
			int page = 1;
			int nbPages = 0;
			if (request.getParameter("page") != null) {
				if (request.getParameter("page").matches("[0-9]{1,3}")) {
					page = Integer.parseInt(request.getParameter("page"));
				}
			}
			request.setAttribute("cPage", page);
			AnnonceMetier am = new AnnonceMetier();
			nbPages = ((int) (Math.ceil((double) am.countAnnonce() / 3)));
			List<Annonce> listAnnonces = am.paginationAnnonce((page - 1) * 3);
			request.setAttribute("annonces", listAnnonces);
			request.setAttribute("pageMax", nbPages);
			getServletContext().getRequestDispatcher("/WEB-INF/home.jsp").forward(request, response);
		} else {
			response.sendRedirect("Login");
		}
	}

}
