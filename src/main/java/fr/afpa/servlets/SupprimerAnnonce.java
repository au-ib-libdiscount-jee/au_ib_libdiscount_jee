package fr.afpa.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Auth;
import fr.afpa.metiers.AnnonceMetier;
import fr.afpa.metiers.AuthMetier;

/**
 * 
 * @author AU_LIBDISCOUNT
 *
 */
/**
 * Servlet implementation class SupprimerAnnonce
 */
public class SupprimerAnnonce extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
    public SupprimerAnnonce() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * Supprime une annonce en récuperant l'id de l'annonce de concerné, puis lance une page de confirmation
	 *  @param l'id annonce
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		AnnonceMetier aa = new AnnonceMetier();
		Annonce annonce  = aa.searchAnnonceParId(id);
		aa.supprimerAnnonce(annonce);
		 getServletContext().getRequestDispatcher("/WEB-INF/Notif.jsp").forward(request, response);

	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 
	}

}
