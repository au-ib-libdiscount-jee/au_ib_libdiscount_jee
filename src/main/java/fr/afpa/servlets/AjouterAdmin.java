package fr.afpa.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.beans.Admin;
import fr.afpa.beans.Auth;
import fr.afpa.beans.Auth_admin;
import fr.afpa.metiers.AdminMetiers;
import fr.afpa.metiers.AuthAdminMetiers;
import fr.afpa.metiers.AuthMetier;

/**
 * 
 * @author AU_LIBDISCOUNT
 *
 */
/**
 * Servlet implementation class AjouterAdmin
 */
public class AjouterAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
    public AjouterAdmin() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

	}

	/**
	 * permet d'ajouter un admin dans la bdd sans l'integrer en dur
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String mail = request.getParameter("email");
		Admin admin = new Admin(nom,prenom,mail);
		
		AdminMetiers adm = new AdminMetiers();
		adm.addAdmin(admin);
		
		new AuthAdminMetiers().addAuth(new Auth_admin(admin,request.getParameter("login") , request.getParameter("mdp")));
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/AjoutAdmin.jsp");
		System.out.print("Admin : " + adm.toString());
		dispatcher.forward(request, response);
	}

}
