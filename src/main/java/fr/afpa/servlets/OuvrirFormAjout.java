package fr.afpa.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 
 * @author AU_LIBDISCOUNT
 *
 */
/**
 * Servlet implementation class OuvrirFormAjout
 */
public class OuvrirFormAjout extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
    public OuvrirFormAjout() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/AjoutAdmin.jsp");
		dispatcher.forward(request, response);
	}

}
