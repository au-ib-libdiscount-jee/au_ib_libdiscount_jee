package fr.afpa.servlets;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Image;
import fr.afpa.beans.Utilisateur;
import fr.afpa.metiers.AnnonceMetier;

/**
 * 
 * @author AU_LIBDISCOUNT
 *
 */
/**
 * Servlet implementation class UpdateAnnonce
 */
public class UpdateAnnonce extends GuestionAnnonce {
	private static final long serialVersionUID = 1L;

	
	public UpdateAnnonce() {
		super();
	}

	/**
	 *Formulaire d'update de l'annonce, r�cupere l'annonce
	 *  @param la session user en cours et ses annonces
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		
		Utilisateur user = (Utilisateur) request.getSession().getAttribute("user");
		if (user != null) {
			String idS = request.getParameter("id");
			if (idS != null && idS.matches("[0-9]+")) {
				Annonce annonce = new AnnonceMetier().getAnnonceByID(Integer.parseInt(idS));
				if (annonce != null) {
					request.getSession().setAttribute("annonce_id", annonce.getIdAnnonce());
					request.setAttribute("annonce", annonce);
					getServletContext().getRequestDispatcher("/WEB-INF/updateAnnonce.jsp").forward(request, response);
				}
			}
		} else {
			response.sendRedirect("index.jsp");
		}
	}

	/**
	 *Verifie si les champs de saisie de l'update ne sont pas null
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		
		Utilisateur user = (Utilisateur) request.getSession().getAttribute("user");
		if (user != null) {
			Object id_Annonce = request.getSession().getAttribute("annonce_id");
			if (id_Annonce != null) {
				AnnonceMetier am = new AnnonceMetier();
				Annonce annonce = am.getAnnonceByID((int) id_Annonce);
				if (annonce != null) {
					ServletFileUpload sf = new ServletFileUpload(new DiskFileItemFactory());
					try {
						List<FileItem> multifiles = sf.parseRequest(request);
						modifierImages(annonce, multifiles);
						if (parseToAnnonce(annonce, multifiles)) {
							am.updateAnnonce(annonce);
						}
					} catch (FileUploadException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			request.getSession().removeAttribute("annonce_id");
			response.sendRedirect("Home");
		} else {
			response.sendRedirect("index.jsp");
		}
	}

	private void modifierImages(Annonce annonce, List<FileItem> multifiles) {
		File dos = new File(getServletContext().getRealPath("") + File.separator + "img_annonces" + File.separator);
		for (FileItem item : multifiles) {
			if (!item.isFormField() && isImageType(item)) {
				String nameField = item.getFieldName();
				if (nameField != null && !"vide".equals(nameField)) {
					File old = new File(dos, nameField);
					if (old.exists()) {
						old.delete();
					}
					Image img = getImageByName(annonce.getImagesList(), nameField);
					if (img != null) {
						img.setNom(deleteExtention(nameField) + "." + getItemFormat(item));
						try {
							item.write(new File(dos, img.getNom()));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				} else {
					String nextIndex = getNextImageIndex(annonce.getImagesList(), "img" + annonce.getIdAnnonce());
					if (nextIndex != null) {
						String nom = nextIndex + "." + getItemFormat(item);
						try {
							item.write(new File(dos, nom));
						} catch (Exception e) {
							e.printStackTrace();
						}
						annonce.getImagesList().add(new Image(nom, annonce));
					}
				}
			}
		}
	}
}
