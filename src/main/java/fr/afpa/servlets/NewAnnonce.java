package fr.afpa.servlets;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Image;
import fr.afpa.beans.Utilisateur;
import fr.afpa.metiers.AnnonceMetier;

/**
 * 
 * @author AU_LIBDISCOUNT
 *
 */
/**
 * Servlet implementation class NewAnnonce
 */
public class NewAnnonce extends GuestionAnnonce {
	private static final long serialVersionUID = 1L;

	
	public NewAnnonce() {
		super();

	}

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Utilisateur user = (Utilisateur) request.getSession().getAttribute("user");
		if (user != null) {
			getServletContext().getRequestDispatcher("/WEB-INF/NewAnnonce.jsp").forward(request, response);
		} else {
			response.sendRedirect("index.jsp");
		}
	}

	/**
	 * Recuperee les valeur dans les champ pour cr�er une annonce, avec en option d'y ajouter une image
	 * @param l'attribut user
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Utilisateur user = (Utilisateur) request.getSession().getAttribute("user");
		if (user != null) {
			ServletFileUpload sf = new ServletFileUpload(new DiskFileItemFactory());
			try {
				List<FileItem> multifiles = sf.parseRequest(request);
				Annonce annonce = new Annonce();
				
				if (parseToAnnonce(annonce, multifiles)) {
					
					annonce.setUtilisateur(user);
					annonce.setDateAnnonce(LocalDate.now());
					AnnonceMetier am = new AnnonceMetier();
					am.addAnnonce(annonce);

					// ** IMAGES
					File dos = new File(
							getServletContext().getRealPath("") + File.separator + "img_annonces" + File.separator);
					if (!dos.exists()) {
						dos.mkdir();
					}
					int count = 0;
					List<Image> listImage = new ArrayList<>(3);
					String nom = "";
					for (FileItem item : multifiles) {
						if (!item.isFormField() && isImageType(item)) {
							nom = "img" + annonce.getIdAnnonce() + (++count) + "." + getItemFormat(item);
							item.write(new File(dos, nom));
							listImage.add(new Image(nom, annonce));
						}
					}

					if (!listImage.isEmpty()) {
						annonce.setImagesList(listImage);
						am.updateAnnonce(annonce);
					}
				}
			} catch (FileUploadException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				response.sendRedirect("Home");
			}
			
		} else {
			response.sendRedirect("index.jsp");
		}
		
	}

}
