package fr.afpa.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.beans.Annonce;
import fr.afpa.metiers.AnnonceMetier;

/**
 * 
 * @author AU_LIBDISCOUNT
 *
 */
/**
 * Servlet implementation class SearchServlet
 */
public class SearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	public SearchServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Permet de gerer la pagination pour la fonction de recherche
	 *  @return pagination
	 *  @param la session user en cours
	 *      
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (request.getSession().getAttribute("user") != null) {
			int page = 1;
			String filter = request.getParameter("filter");
			String value = request.getParameter("value");
			String pageS = request.getParameter("page");
			if (pageS != null && pageS.matches("[0-9]{1,3}")) {
				page = Integer.parseInt(request.getParameter("page"));
			}

			if ("".equals(filter)) {
				response.sendRedirect("Home");
			} else {

				List<Annonce> results = new ArrayList<Annonce>();
				if (filter != null && !"".equals(value)) {
					AnnonceMetier am = new AnnonceMetier();
					value = value.toLowerCase();

					int nbPages = ((int) (Math.ceil((double) am.countAnnonce(filter, value) / 3)));
					results = am.paginationAnnonce((page - 1) * 3, filter, value);

					request.setAttribute("cPage", page);
					request.setAttribute("pageMax", nbPages);
					request.setAttribute("filter", filter);
					request.setAttribute("value", value);
				}
				request.setAttribute("annonces", results);
				getServletContext().getRequestDispatcher("/WEB-INF/home.jsp").forward(request, response);

			}
		} else {
			response.sendRedirect("Login");
		}
	}
}
