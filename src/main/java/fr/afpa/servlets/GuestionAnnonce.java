package fr.afpa.servlets;

import java.time.LocalDate;
import java.util.List;

import javax.servlet.http.HttpServlet;

import org.apache.commons.fileupload.FileItem;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Image;

/**
 * 
 * @author AU_LIBDISCOUNT
 *
 */
/**
 * Servlet implementation class GuestionAnnonce
 */
public abstract class GuestionAnnonce extends HttpServlet {
	private static final long serialVersionUID = 1L;
	

	
	public GuestionAnnonce() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected boolean isImageType(FileItem item) {
		String format = getItemFormat(item);
		return format.equals("jpeg") || format.equals("png");
	}

	protected String getItemFormat(FileItem item) {
		return item.getContentType().split("/")[1];
	}
	/**
	 * Compte le nombre d'attribut et verrifie si l'annonce possede bien les _
	 * @return nombre d'attribut
	 * 
	 */

	protected boolean parseToAnnonce(Annonce annonce,List<FileItem> multiFiles) {
		int count = 0;
		String value = "";
		for (FileItem item : multiFiles) {
			if (item.isFormField() && !"".equals((value = item.getString()))) {

				switch (item.getFieldName()) {
				case "titre":
					annonce.setTitre(value);
					count++;
					break;
				case "isbn":
					annonce.setIsbn(value);
					count++;
					break;
				case "medition":
					annonce.setMaisonEdition(value);
					count++;
					break;
				case "dedition":
					annonce.setDateEdition(LocalDate.parse(value));
					count++;
					break;
				case "niveau_scolaire":
					annonce.setNiveau_scolaire(value);
					count++;
					break;
				case "remise":
					annonce.setRemise(Float.parseFloat(value));
					count++;
					break;
				case "qte":
					annonce.setQuantite(Integer.parseInt(value));
					count++;
					break;
				case "prix_u":
					annonce.setPrixUnitaire(Float.parseFloat(value));
					count++;
					break;
				default:
					break;
				}
			}
		}
		return count == 8;
	}

	protected String deleteExtention(String fileName) {
		return fileName.split("\\.")[0];
	}

	protected Image getImageByName(List<Image> list, String name) {
		for (Image img : list) {
			if (img.getNom().equals(name)) {
				return img;
			}
		}
		return null;
	}

	protected String getNextImageIndex(List<Image> list, String patern) {
		String name = null;
		boolean check;
		for (int i = 0; i < 3; i++) {
			check = true;
			name = patern+(i+1);
			for (Image img : list) {
				if (img.getNom().startsWith(name)) {
					check &= false;
					break;
				}
			}
			if(check) {
				return name;
			}
		}
		return null;
	}
}
