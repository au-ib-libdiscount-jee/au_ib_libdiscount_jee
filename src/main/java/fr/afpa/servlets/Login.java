package fr.afpa.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.beans.Auth;
import fr.afpa.beans.Utilisateur;
import fr.afpa.metiers.AuthMetier;

/**
 * 
 * @author AU_LIBDISCOUNT
 *
 */
/**
 * Servlet implementation class Login
 */
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	public Login() {
		super();

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (request.getSession().getAttribute("user") != null) {
			response.sendRedirect("Home");
		} else {
			getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
		}
	}

	/**
	 *Formulaire de connexion
	 *Cr�er une session
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Auth auth = new AuthMetier().getAuth(request.getParameter("login"), request.getParameter("pwd"));

		if (auth != null) {

			Utilisateur user = auth.getUtilisateur();

			HttpSession session = request.getSession();

			session.setAttribute("user", user);

			response.sendRedirect("Home");
		} else {
			response.sendRedirect("index.jsp");
		}

	}

}
