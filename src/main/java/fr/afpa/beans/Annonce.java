package fr.afpa.beans;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@ToString
@NamedQuery(name = "findAllById", query = "from Annonce u where u.utilisateur.id_user = :id")
@NamedQuery(name = "findByTitre", query = "from Annonce u where lower(u.titre) like :motCle")
@NamedQuery(name = "findByISBN", query = "from Annonce u where u.isbn = :isbn")
@NamedQuery(name = "findByVille", query = "from Annonce u where lower(u.utilisateur.adresse.ville) = :ville")
@NamedQuery(name = "findByIdAnnonce", query = "from Annonce a where a.id = :id")
public final class Annonce {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "annonce_gen")
	@SequenceGenerator(name = "annonce_gen", sequenceName = "annonce_seq")
	@Column(name = "id_annonce")
	private int idAnnonce;

	@Column(nullable = false)
	private LocalDate dateAnnonce;
	
	@Column(nullable = false)
	private String titre;
	
	@Column(nullable = false)
	private String niveau_scolaire;

	@Column(nullable = false)
	private String maisonEdition;
	
	@Column(nullable = false)
	private LocalDate dateEdition;

	@Column(nullable = false)
	private String isbn;

	@Column(nullable = false)
	private float prixUnitaire;

	@Column(nullable = false)
	private int quantite;

	@Column(nullable = false)
	private float remise;

	@ManyToOne
	@JoinColumn(name = "id_user", referencedColumnName = "id_user")
	private Utilisateur utilisateur;

	@OneToMany(mappedBy = "annonce", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<Image> imagesList;
}
