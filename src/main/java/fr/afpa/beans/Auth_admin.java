package fr.afpa.beans;


import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@NamedQuery(name = "findByLoginAndPassword", query = "FROM Auth_admin a WHERE a.login = :login AND a.password = :password")

public class Auth_admin implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4871812202615451972L;
	@Id
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id", referencedColumnName = "id")
	private Admin admin;
	@Column(nullable = false)
	private String login;
	@Column(nullable = false)
	private String password;
	

	public String toString() {
		return "Login ; " + login + " , password ; " + password;
	}

}
