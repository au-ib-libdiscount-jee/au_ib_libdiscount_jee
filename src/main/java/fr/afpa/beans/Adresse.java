package fr.afpa.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@ToString
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
public class Adresse {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "adresse_gen")
	@SequenceGenerator(name = "adresse_gen" ,sequenceName = "adresse_seq")
	@Column(name = "id_adresse")
	private int idAdress;

	@NonNull
	@Column(nullable = false)
	private String numero;

	@NonNull
	@Column(nullable = false)
	private String libelle;

	@NonNull
	@Column(nullable = false)
	private String cp;

	@NonNull
	@Column(nullable = false)
	private String ville;
}
