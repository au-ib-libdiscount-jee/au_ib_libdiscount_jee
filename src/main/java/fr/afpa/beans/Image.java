package fr.afpa.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
public class Image {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "image_gen")
	@SequenceGenerator(name = "Image_gen" ,sequenceName = "image_seq")
	private int id;

	@NonNull
	@Column(nullable = false)
	private String nom;
	
	@NonNull
	@ManyToOne
	@JoinColumn(name = "id_annonce")
	private Annonce annonce;
}
