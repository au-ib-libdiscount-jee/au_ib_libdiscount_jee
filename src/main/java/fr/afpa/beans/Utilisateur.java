package fr.afpa.beans;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@NamedQuery(name = "allUser", query = "From Utilisateur")
@NamedQuery(name = "FindByIdUser", query = "From Utilisateur u WHERE u.id_user = :id")
public class Utilisateur {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "utilisateur_gen")
	@SequenceGenerator(name = "utilisateur_gen", sequenceName = "utilisateur_seq")
	@Column(name = "id_user")
	private int id_user;

	@NonNull
	@Column(nullable = false)
	private String nom;

	@NonNull
	@Column(nullable = false)
	private String prenom;

	@NonNull
	@Column(nullable = false)
	private String email;

	@NonNull
	@Column(nullable = false)
	private String num_tel;

	@NonNull
	@Column(nullable = false)
	private String nom_librairie;

	@NonNull
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_adresse", referencedColumnName = "id_adresse")
	private Adresse adresse;

	@OneToMany(mappedBy = "utilisateur")
	private List<Annonce> listeAnnonce;

	@Override
	public String toString() {
		return "Utilisateur [id_user=" + id_user + ", nom=" + nom + ", prenom=" + prenom + ", email=" + email
				+ ", num_tel=" + num_tel + ", nom_librairie=" + nom_librairie + ", adresse=" + adresse + "]";
	}
}
