package fr.afpa.beans;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@NamedQuery(name = "FindByEmail", query = "FROM Auth a WHERE a.email = :email")
@NamedQuery(name = "FindAuthByIdUser", query = "FROM Auth a WHERE a.utilisateur.id_user = :id")
@NamedQuery(name = "findByEmailAndPassword", query = "FROM Auth a WHERE a.email = :email AND a.password = :password and active = true")
@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Auth implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4501033209924926121L;

	@Id
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_user", referencedColumnName = "id_user")
	private Utilisateur utilisateur;

	@Column(unique = true, nullable = false)
	private String email;

	@Column(nullable = false)
	private String password;

	@Column(nullable = false)
	private boolean active;

}
